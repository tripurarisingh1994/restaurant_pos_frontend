import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private myRoute: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if ( localStorage.getItem('currentUser')) {
          console.log('auth guard ' + 'if');
          return true;
      } else {
          this.myRoute.navigate(['/auth'], {
              queryParams: {
                  return: state.url,
              },
          });
          console.log('auth guard else');
          return false;
      }
  }
}
