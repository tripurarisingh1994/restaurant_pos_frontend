import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './_gurad/auth.guard';
import { AuthService } from './auth/auth.service';
import { CommonService } from './_service/common.service';
import { FoodCategoryResolver } from './pages/table/food-cats.resolver.service';
import { TableCategoryResolver } from './pages/table/table-cats.resolver.service';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
  ],
  bootstrap: [AppComponent],
  providers: [
     AuthGuard,
     AuthService,
     CommonService,
     FoodCategoryResolver,
     TableCategoryResolver,
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppModule {
}
