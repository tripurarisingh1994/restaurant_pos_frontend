import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService, private myRoute: Router) { }

  ngOnInit() {

  }

  submit(email, password) {
      this.auth.login(email, password).subscribe(data => {

          if (data['message'] === 'success') {
            console.log(JSON.stringify(data));
            localStorage.setItem('currentUser', JSON.stringify(data));
            this.myRoute.navigate(['pages/dashboard']);
          } else {
              alert('Email or password wrong');
          }
      });
  }

}
