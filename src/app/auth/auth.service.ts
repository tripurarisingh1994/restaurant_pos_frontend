import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ConfigService } from '../_service/config.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  // private url:string='https://ydias.in';

  constructor(private http: HttpClient, private configService: ConfigService) { }

  public login(email, password) {

    const data = new FormData();
    data.append('email', email);
    data.append('password', password);
    return this.http.post(`${this.configService.url}/restaurant/index.php/auth/doLogin`, data);
  }
}
