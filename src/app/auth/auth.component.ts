import { Component } from '@angular/core';


@Component({
    selector: 'ngx-components',
    styleUrls: ['./auth.component.scss'],
    template: `
        <nb-layout>
            <nb-layout-column center>
                <router-outlet></router-outlet>
            </nb-layout-column>
        </nb-layout>
  `,
})
export class AuthComponent {
}
