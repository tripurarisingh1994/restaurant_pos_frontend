import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';



interface Cart {
  id:number,
  name:string,
  price:number,
  qty:number
}


// interface Kot {
//   tableCategory: string,
//   tableNumber: number,
//   customer: {},
//   selectedItems: Object[],
//   totalPrice: number
//   created_at?: string
// }


@Injectable({
  providedIn: 'root',
})

export class CommonService {

  // private url:string='https://ydias.in';

  /** Storing the food items */
 public items_cart: Cart[] = []; 

 /* Store the id of the food items inseted in cart for update the qty of the item */
//  public id_cart: number;

 /** Store Kot data */

//  public kotData: Array<Kot> = [];

 constructor(private http: HttpClient, private configService: ConfigService) { }

  /**
  * Calculate the Total Price 
  * items added in cart 
   */
 totalPriceOfFoodItems(): number {

    if(this.items_cart.length > 0) {
      let price:number=0;
        for(let i=0; i<this.items_cart.length; i++) {
           price += this.items_cart[i].price * this.items_cart[i].qty;
        }

        return price;
    }

    return 0;
  }  
  
  /** Getting table category and inside total table */

  getTableCategory() {
    return this.http.get(`${this.configService.url}/index.php/common/get_TableCategory_With_TableNo`);
  }

  /** Getting Food Category */

  getFoodCategory() {
    return this.http.get(`${this.configService.url}/index.php/common/getFoodCategory`);
  }

  /** Getting the food items by food category id */
  getFoodItemsByFoodCategory(id) {
    return this.http.get(`${this.configService.url}/index.php/common/getFoodItemsByFoodCatId?id=${id}`);
  }

  /** Getting the price of requested food item by food item id */

  getFoodItemPriceByFoodItemId(id) {
    return this.http.get(`${this.configService.url}/index.php/common/getFoodItemPriceByFoodItemId?id=${id}`);
  }

  /** Saving Customer Details */

  savingCustomerDetails(name, email, mob) {
      const customer = new FormData();
      customer.append('name', name);
      customer.append('email', email);
      customer.append('mob', mob);

      return this.http.post(`${this.configService.url}/index.php/common/saveCustomersDetails`, customer);
  }

  /** Search the customer */
  
  searchCustomer(query) {
    return this.http.get(`${this.configService.url}/index.php/common/searchCustomerByName?query=${query}`);
  }

  /** Search the Food Items by item name */
  searchFoodItems(query) {
    return this.http.get(`${this.configService.url}/index.php/common/searchFoodItemsByName?query=${query}`)
  }

  /** Insert new invoice Data */

  insertNewInvoice(customer_id, total_price) {
    const newInvoice = new FormData();
    newInvoice.append('customer_id', customer_id);
    newInvoice.append('total_price', total_price);

    return this.http.post(`${this.configService.url}/index.php/common/insertInvoiceData`, newInvoice);
  }
}
