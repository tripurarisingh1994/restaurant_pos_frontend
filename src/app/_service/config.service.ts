import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  
  // public url: string = "http://localhost";
  public url: string = "https://18.225.21.50/restaurant";

  constructor() {}
}
