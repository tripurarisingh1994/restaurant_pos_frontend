import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { CommonService } from "../../_service/common.service";

@Injectable()

export class FoodCategoryResolver implements Resolve<any> {

    constructor(private commonService: CommonService) {}

    resolve(): Observable<any> {
        return this.commonService.getFoodCategory();
    }
}
