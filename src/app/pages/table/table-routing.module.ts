import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table.component';
import { SetTableComponent } from './set-table/set-table.component';
import { BookTableComponent, NbGeneralDialogComponent, NbDialogChangeTableComponent } from './book-table/book-table.component';
import { TableCategoryResolver } from './table-cats.resolver.service';
import { FoodCategoryResolver } from './food-cats.resolver.service';


const routes: Routes = [{
  path: '',
  component: TableComponent,
  children: [{
      path: '',
      component: SetTableComponent,
      resolve: { tableCategory: TableCategoryResolver},
   },
   {
    path: 'set-table',
    component: SetTableComponent,
    resolve: { tableCategory: TableCategoryResolver},
 },
 {
  path: 'book-table',
  component: BookTableComponent,
  resolve: { foodCategory: FoodCategoryResolver},
}],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [
    NbGeneralDialogComponent,
    NbDialogChangeTableComponent
  ]
})
export class TableRoutingModule { }

export const routedComponents = [
  TableComponent,
  SetTableComponent,
  BookTableComponent,
  NbGeneralDialogComponent,
  NbDialogChangeTableComponent
];
