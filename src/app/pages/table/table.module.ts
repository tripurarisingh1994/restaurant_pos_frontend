import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { TableRoutingModule, routedComponents } from './table-routing.module';
import { NbListModule, NbButtonModule, NbSpinnerModule, NbDialogModule, NbInputModule, NbActionsModule, NbToastrModule } from '@nebular/theme';

@NgModule({
    imports: [
        ThemeModule,
        TableRoutingModule,
        NbListModule,
        NbButtonModule,
        NbSpinnerModule,
        NbInputModule,
        NbActionsModule,
        NbDialogModule.forChild(),
        NbToastrModule.forRoot(),
    ],
    declarations: [
       ...routedComponents,
    ],
})
export class TableModule { }
