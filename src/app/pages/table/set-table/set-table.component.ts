import { Component, OnInit, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-set-table',
  templateUrl: './set-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./set-table.component.scss']
})
export class SetTableComponent implements OnInit,  AfterViewInit{

  tbl_cats = [];
  kotData = [];  // saving the kot data

  constructor(private _route: ActivatedRoute, private myRoute: Router) {
    console.log('SetTableComponent');
   }

  ngOnInit() {
    this.getTableCategory();
  }

  getTableCategory(): void {

    this.tbl_cats = this._route.snapshot.data['tableCategory'].data;
    console.log(this.tbl_cats);
  }

  createDummyArray(len): Array<any> {
    
    const _len = Number(len);
    const arr: number[] = [];
      for (let i = 0; i < _len ; i++) {
          arr.push(i + 1);
      }
      return arr;
  }
  

  /** When click on table 
   *  go to the book-table 
   *  page (assignTheTableToTheCustomer)
   */

  assignTheTableToCustomer(tblData, tblNo): void {
    console.log("Table Data ",tblData);
    console.log("Table No ",tblNo);

    const kotDL = JSON.parse(localStorage.getItem('kotData'));
    if( kotDL != null) {
      const _index = kotDL.findIndex(items => items.tableNumber == tblNo && items.tableCategory == tblData.table_cat_name);

      if(_index > -1) {

        this.myRoute.navigate(['/pages/table/book-table', { tbl_cate: tblData.table_cat_name, tbl_no: tblNo, tbl_status: 'booked' } ]);
      }
      else {
        
        this.myRoute.navigate(['/pages/table/book-table', { tbl_cate: tblData.table_cat_name, tbl_no: tblNo } ]);
      }

    }
    else {
      this.myRoute.navigate(['/pages/table/book-table', { tbl_cate: tblData.table_cat_name, tbl_no: tblNo } ]);
    }
  }

  ngAfterViewInit() {
    const _kotDataL = JSON.parse(localStorage.getItem('kotData'));
    if(_kotDataL == null) {
      console.log('if ngAfterViewInit');
    }
    else {
      console.log('else inside the ngAfterViewInit');
      this.kotData = _kotDataL;
      console.log("kotData", this.kotData);
    }
  }

}
