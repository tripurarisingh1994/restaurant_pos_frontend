import { Component, } from '@angular/core';

@Component({
  selector: 'ngx-table',
  template: `
  <router-outlet></router-outlet>
  `,
})

export class TableComponent {

}
