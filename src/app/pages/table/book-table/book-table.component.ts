import { Component, OnInit, AfterContentChecked, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CommonService } from '../../../_service/common.service';
import { NbDialogRef, NbDialogService, NbToastrService } from '@nebular/theme';
import { debounceTime } from 'rxjs/operators';


interface IKot {
  tableCategory: string,
  tableNumber: number,
  customer: {},
  selectedItems: Object[],
  totalPrice: number
  created_at?: string
}

/******************************************************  General Modal  Start *****************************************************/


@Component({
  selector: 'nb-dialog',
  template: `
    <nb-card accent="warning" [style.width.px]="600" [style.height.px]="370">
      <nb-card-header><h4>{{ title }}</h4></nb-card-header>
      <nb-card-body>
          <div class="row">
            <div class="col-md-6">
                <label for="name">Name <span class="label-star">*</span></label>
                <input type="text" #name nbInput fullWidth id="name" placeholder="Enter the customer name">
            </div>
            <div class="col-md-6">
                <label for="email" class="email-label">Email </label>
                <input type="email" #email nbInput fullWidth id="email" placeholder="Enter the customer email">
            </div>
          </div>
          <div class="row" [style.margin-top.px]="24">
              <div class="col-md-6">
                  <label for="mobile">Mobile <span class="label-star">*</span></label>
                  <input type="tel" #mob nbInput fullWidth id="mobile" placeholder="Enter the customer mobile">
              </div>
          </div>
      </nb-card-body>
      <nb-card-footer>
        <div [ngStyle]="{'float':'right'}">
          <button nbButton hero status="danger" (click)="dismiss()">close</button>
          <button nbButton hero status="success" [disabled]="!name.value || !mob.value" (click)="saveCustomerDetails(name.value, email.value, mob.value)">Save</button>
        </div>
      </nb-card-footer>
    </nb-card>
  `,
  styles:[
    `
     label {
        color: #fff;
     }
     nb-card-footer div button:nth-child(2){
       margin: 0 10px;
     }

     nb-card-footer div button {
       cursor: pointer;
     }

    .label-star {
        color: #ff386a;
        font-size: 1.6em;
        font-weight: 650;
        margin-left: 1px;
    }

    .email-label {
      margin-top: 0.6em;
      font-size: 1.05em;
    }
    
    `
  ], 
})
export class NbGeneralDialogComponent {
  @Input() title: string;

  constructor(protected ref: NbDialogRef<NbGeneralDialogComponent>,
              private commonService: CommonService,
              private toastrService: NbToastrService) {
  }

  dismiss() {
    this.ref.close();
  }

  saveCustomerDetails(name, email, mob): void {
    this.commonService.savingCustomerDetails(name, email, mob).subscribe(data=>{ 
      console.log(data);
      if(data['message']=='success') {
        this.toastrService.success('Success','Data added successfully');
      }
      else {
        this.toastrService.warning('Warning', 'Getting problem in data saving'); 
      }
    },
    (err)=>console.log(err)
    );

    this.ref.close();
  }
}



/************************************    End Of Modal   **************************************/

/************************************   Change  the Table ***********************************/ 

@Component({
  selector: 'nb-changetable-prompt',
  template: `
    <nb-card>
      <nb-card-header>Change Table</nb-card-header>
      <nb-card-body>
        <input style="margin: 0 2.3rem;" #table nbInput placeholder="Table No">
      </nb-card-body>
      <nb-card-footer>
        <button nbButton hero status="danget" (click)="cancel()">Cancel</button>
        <button nbButton hero status="success" [disabled]="!table.value" (click)="changeTheTable(table.value)">Update</button>
      </nb-card-footer>
    </nb-card>
  `,
  styles: [`
    button {
      margin: 1rem;
      cursor:pointer !important;
    }
  `],
})

export class NbDialogChangeTableComponent implements OnInit{

  @Input() tblCate: string;
  @Input() tblNo: string; 
 

  constructor(protected dialogRef: NbDialogRef<NbDialogChangeTableComponent>,
    private location: Location, private commonService: CommonService) { 

     }

  ngOnInit() {
    console.log("tblCate ",this.tblCate);
    console.log("tblNo ",this.tblNo)
  }

  cancel(): void {
    this.dialogRef.close();
  }

  changeTheTable(tableNo): void {
    const tblNo = parseInt(tableNo);
    // console.log("ChangeTheTable tblNo ",tblNo);
    // console.log("type of", typeof tblNo);

    let localKotData = [];
    localKotData = JSON.parse(localStorage.getItem('kotData'));

    if(localKotData != null && typeof tblNo == 'number') {
      // console.log(localKotData);
      const _index = localKotData.findIndex(items => items.tableNumber == this.tblNo && items.tableCategory == this.tblCate)

      localKotData[_index].tableNumber = tblNo;
      localStorage.removeItem('kotData');
      localStorage.setItem('kotData', JSON.stringify(localKotData));

      this.commonService.items_cart = [];
      
      this.location.back();
    }
    else {
      console.log('table no and table category not matched');
    }

    this.dialogRef.close(tblNo);

  }
}


@Component({
  selector: 'ngx-book-table',
  templateUrl: './book-table.component.html',
  styleUrls: ['./book-table.component.scss']
})

export class BookTableComponent implements OnInit,  AfterContentChecked {

  loading: boolean = false;
  food_cat_lists = [];
  food_items = [];
  foodItems_Added_In_Cart:Array<Object>=[];
  totalPrice:number = 0; 

  tblCate: string;
  tblNo: string;
  tblStatus: string;

  dateTime: string;   // save for kot
  rePrintKot_dateTime: string; // save the re print kot

  filteredCustomerList = [];    // to store the searched result of customers

  isSelectCustomer: boolean = false;

  filteredFoodItemsList = [];   // to store the searched result of food items 

  selectedCustomerData = {};   // store the selected customer name from search filter list

  printBillData:IKot[] = [];   // saving for the  print bill data 

  selectedItemsForPrint = [];   // saving the selectedItems for the print bill data


  constructor(private _route: ActivatedRoute,
              private commonService: CommonService,
              private location: Location,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService) {

    console.log('BookTableComponent');
    this.dateTime = new Date().toLocaleString();
   }

  ngOnInit() {
    /** Getting Route Params */
     this.tblCate = this._route.snapshot.paramMap.get('tbl_cate');
     this.tblNo = this._route.snapshot.paramMap.get('tbl_no');
     this.tblStatus = this._route.snapshot.paramMap.get('tbl_status');

     console.log("Table Category ",this.tblCate);
     console.log("Table No ",this.tblNo);
     console.log("Table status ", this.tblStatus);

    this.getFoodCategoryList();
    this.comeFromReservedTable(); 
  }

  /** When come through reserved table then update the cart */
  comeFromReservedTable() {
    const cartDataFromLocal = JSON.parse(localStorage.getItem('kotData'));

    if(cartDataFromLocal != null) {
      const _index = cartDataFromLocal.findIndex(items => items.tableNumber == this.tblNo && items.tableCategory == this.tblCate);

      if(_index > -1) {
        console.log("cartDataFromLocal ",cartDataFromLocal[_index]);
        this.commonService.items_cart = cartDataFromLocal[_index].selectedItems;

        // cutomer name  set

        if(cartDataFromLocal[_index].customer.id != undefined) {
          this.isSelectCustomer = true;
          this.selectedCustomerData = cartDataFromLocal[_index].customer;
        }

        // Re - Print Kot Date initilisation
        this.rePrintKot_dateTime = cartDataFromLocal[_index].created_at;
        this.printBillData = cartDataFromLocal[_index];
        this.selectedItemsForPrint = cartDataFromLocal[_index].selectedItems;
      }
      else {
          console.log('This table is not reserved');
      }
    }
    else {
        console.log('Kot Data is null');
    }
  }

  getFoodCategoryList(): void {
    this.food_cat_lists = this._route.snapshot.data['foodCategory'].data;
    console.log(this.food_cat_lists[0].id);
    this.getFoodItemsByFoodCatId(this.food_cat_lists[0].id);
  }

  getFoodItemsByFoodCatId(id): void {
    this.loading = true;
    this.commonService.getFoodItemsByFoodCategory(id).subscribe(data => {
       console.log(data['data']);
       this.food_items = data['data'];
       this.loading = false;
    });
  }

  /** Adding Food Items in the cart  */

  foodItemsAddToCart(item): void {
     console.log(item);
     let cart;

     this.commonService.getFoodItemPriceByFoodItemId(item.id).subscribe( async  data => {

         cart = {
          id    : parseInt(item.id),
          name  : item.name,
          qty   : 1,
          price : await parseFloat(data['data'][0].price)
        }
          await this.commonService.items_cart.push(cart);

          this.foodItems_Added_In_Cart = this.commonService.items_cart;

          // this.totalPrice += parseFloat(data['data'][0].price);
          this.totalPrice = this.commonService.totalPriceOfFoodItems();
     });

  }

  /** Delete cart item by item id */

  deleteFoodItem_FromCart(cart): void {
    console.log(cart);
    const index = this.commonService.items_cart.findIndex(items => items.id == cart.id);
    console.log("index ",index)
    this.commonService.items_cart.splice(index,1);
    // this.totalPrice -= cart.price;
    this.totalPrice = this.commonService.totalPriceOfFoodItems();

    console.log("deleteFoodItem_FromCart ",this.commonService.items_cart);
  }

  /**
   * When click on Back Button then go to previous page
   */
  backToPreviousPage(): void {
    this.commonService.items_cart = [];
    this.location.back();
  }

/** to change the Quantity of the food items in cart*/
   toChangeQty(id, signFlag): void {
    // this.commonService.id_cart = id;
    // this.dialogService.open(NbDialogQtyPromptComponent)
    console.log("update id ",id, typeof id);

    if(signFlag === 'plus') {
      const index = this.commonService.items_cart.findIndex(items => items.id == id);    
      this.commonService.items_cart[index].qty++;
    }
    else {
      const index = this.commonService.items_cart.findIndex(items => items.id == id);    
      this.commonService.items_cart[index].qty--;
    }

    console.log("update cart ",this.commonService.items_cart);
  }

  /** Open the modal for add customer */
  openAddCustomerModal(): void {
    console.log("add customer");
    this.dialogService.open(NbGeneralDialogComponent, {
      context: {
        title: 'Add Customer',
      },
    });
  }

  /** Change the Table Dialog */
  changeTableDialogOpen(): void {
    if(this.tblStatus == 'booked')
        this.dialogService.open(NbDialogChangeTableComponent,{
          context: {
            tblCate: this.tblCate,
            tblNo: this.tblNo
          },
        });
  }

  /** Search the customer  */

  searchCustomer(query: string): void {
    console.log(query);
    this.filteredCustomerList = [];
    query!='' && this.commonService.searchCustomer(query).pipe(debounceTime(500)).subscribe( data=> {
      console.log(data);
      if(data['message']=='success') 
          this.filteredCustomerList = data['data'];
    }, (err)=> console.log(err));
  }

  /** Select Customer From Search Filter List */
  selectCustomerFromSearchFilter(filterCust): void {

    this.selectedCustomerData = filterCust;

    this.filteredCustomerList = [];

    this.isSelectCustomer = true;

    console.log(this.selectedCustomerData);
  }

  /** Remove the Selected Customer from Search Filter List */

  removeSelectedCustomer(): void {
    this.selectedCustomerData = {};
    this.isSelectCustomer = false;
  }

  
  /**
   * Search Food Items By Item name
   */

   searchFoodItemsByName(query): void {
    console.log(query);
    this.filteredFoodItemsList = [];
    query!='' && this.commonService.searchFoodItems(query).pipe(debounceTime(500)).subscribe(data => {
        console.log(data);
        if(data['message']=='success')
          this.filteredFoodItemsList = data['data'];
    }, (err) => console.log(err));
   }

   /** Select Food Item from Search filter list */
   selectFoodItemFromSearchFilter(filterItem): void {
    console.log(filterItem);
    this.filteredFoodItemsList = [];
   }


  ngAfterContentChecked() {
    this.foodItems_Added_In_Cart = this.commonService.items_cart;
    this.totalPrice = this.commonService.totalPriceOfFoodItems();
  }


  /** Cancel Order Button clik in footer */
  cancelOrder(): void {

    let cartDataFromLocal = [];
        cartDataFromLocal = JSON.parse(localStorage.getItem('kotData'));

    if(cartDataFromLocal != null && this.tblStatus== 'booked') {
      const _index = cartDataFromLocal.findIndex(items => items.tableNumber == this.tblNo && items.tableCategory == this.tblCate);

      if(_index > -1) {
        console.log("cartDataFromLocal ",cartDataFromLocal[_index]);
        cartDataFromLocal.splice(_index,1);
        localStorage.removeItem('kotData');

        localStorage.setItem('kotData', JSON.stringify(cartDataFromLocal));
        this.commonService.items_cart = [];
        this.location.back();
      }
      else {
          console.log('This table is not reserved');
      }
    }
    else {
        console.log('Local Kot Data is null');
    }
  }


  /** Print KOT */

  kot(): void {
  
    if(this.foodItems_Added_In_Cart.length > 0) {

        let divToPrint = document.getElementById('divKotPrint').innerHTML;
        let newWindow = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        newWindow.document.open();
        newWindow.document.write(`
        <html>
            <head>
              <title>Abc Restaurant</title>
              <style>
              
              </style>
            </head>
            <body onload="window.print();window.close()">${divToPrint}   
            </body>
          </html>
        `);
         newWindow.document.close();

        

         const kotData = {
           tableCategory: this.tblCate,
           tableNumber:   parseInt(this.tblNo),
           customer:      this.selectedCustomerData,
           selectedItems: this.foodItems_Added_In_Cart,
           totalPrice:    this.totalPrice,
           created_at:    this.dateTime
         }

        let kot: Array<IKot> = [];

        const _kotDataL = JSON.parse(localStorage.getItem('kotData'));


        if(_kotDataL == null) {
            const _len = kot.push(kotData);
            _len > 0 && localStorage.setItem('kotData', JSON.stringify(kot));
        }
        else {
                    kot = _kotDataL;
            const   _i  = kot.findIndex(items => items.tableNumber == parseInt(this.tblNo) && items.tableCategory == this.tblCate);

            if(_i > -1) {
              console.log("Already Requested for this Table and Items requested to kot");
              kot.splice(_i, 1);
              kot.push(kotData);
              localStorage.removeItem('kotData');
              localStorage.setItem('kotData', JSON.stringify(kot));
            }
            else {
              kot.push(kotData);
              localStorage.setItem('kotData', JSON.stringify(kot));
            }
        }

        this.commonService.items_cart = []; // Reset the Foor items cart
        this.location.back(); // Back to the previous page
    }
    else {
       console.log('else');
       this.toastrService.warning('Warning', 'Please add some items in the cart'); 
    }


  }


  /** Re Print KOT */

  rePrintKot(): void {
    console.log('rePrintKot');
    let cartDataFromLocal = JSON.parse(localStorage.getItem('kotData'));
    const _index = cartDataFromLocal.findIndex(items => items.tableNumber == this.tblNo && items.tableCategory == this.tblCate);

    if(_index > -1) {

        cartDataFromLocal[_index].customer = this.selectedCustomerData;
        localStorage.removeItem('kotData');
        localStorage.setItem('kotData', JSON.stringify(cartDataFromLocal));

        let divToPrint = document.getElementById('divKotPrint').innerHTML;
        let newWindow = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        newWindow.document.open();
        newWindow.document.write(`
        <html>
            <head>
              <title>Abc Restaurant</title>
              <style>
              
              </style>
            </head>
            <body onload="window.print();window.close()">${divToPrint}   
            </body>
          </html>
        `);
         newWindow.document.close();
    }
    else {
        console.log('not Data for this table and category');
    }

    this.location.back();     // Back to the previous page
  }

  printBillToCustomer(): void {

    let cartDataFromLocal = [];

    cartDataFromLocal = JSON.parse(localStorage.getItem('kotData'));

    const _index = cartDataFromLocal.findIndex(items => items.tableNumber == this.tblNo && items.tableCategory == this.tblCate);

    if(_index > -1 && this.tblStatus == 'booked') {

        let billToPrint = document.getElementById('printBill').innerHTML;
        let newWindow = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');

        newWindow.document.open();
        newWindow.document.write(`
        <html>
            <head>
              <title>Abc Restaurant</title>
              <style>
              
              </style>
            </head>
            <body onload="window.print();window.close()">${billToPrint}   
            </body>
          </html>
        `);
         newWindow.document.close();

         console.log("cartDataFromLocal ",cartDataFromLocal);
         console.log("customer id ",cartDataFromLocal[_index].customer.id);
         console.log("Total Price ",cartDataFromLocal[_index].totalPrice);

         this.commonService.insertNewInvoice(cartDataFromLocal[_index].customer.id, cartDataFromLocal[_index].totalPrice).subscribe( async data => {

          if(data['message'] == 'success') {
            this.toastrService.success('Success','Invoice created successfully'); 

                await cartDataFromLocal.splice(_index,1);
                await localStorage.removeItem('kotData');
                
                await localStorage.setItem('kotData', JSON.stringify(cartDataFromLocal));

                 this.commonService.items_cart = await [];    // Reset the cart items

                await this.location.back();
          }
          else{
            this.toastrService.success('Warning','Invoice is not created, something went wrong');

                await cartDataFromLocal.splice(_index,1);
                await localStorage.removeItem('kotData');
                
                await localStorage.setItem('kotData', JSON.stringify(cartDataFromLocal));

                this.commonService.items_cart = await [];    // Reset the cart items

                await this.location.back();
          }

         }, (err)=> console.log(err));

    }
    else {
        console.log('not Data for this table and category');
    }

  }

}
